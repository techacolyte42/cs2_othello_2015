#include "movetreenode.h"

using namespace std;

/**
 * By storing playerside and board side we can do alt min max
 */
MovetreeNode::MovetreeNode(Board * board, Side playerSide, Side side) {

    heuristic = microsoft_heuristic;
    this -> board = board;
    this -> playerSide = playerSide;
    this -> side = side;
    score = board -> scoreBoard(side, heuristic);
}

MovetreeNode::~MovetreeNode() {
    for (MovetreeNode* i : child_nodes) {
//	delete i;
    }
    //delete board;
}

int MovetreeNode::getScore() {
    return score; // score will be min max (propogated up)
}

Board *MovetreeNode::getBoard() {
    return board;
}

Move *MovetreeNode::getBestMove() {

    Move * bestMove = NULL;
    int bestScore = -10000;

    for (unsigned int i = 0; i < child_moves.size(); i++) {
	if (child_nodes[i] -> getScore() > bestScore) {
	    bestScore = child_nodes[i] -> getScore();
	    bestMove = child_moves[i];
	}
    }

    return bestMove;
}

MovetreeNode *MovetreeNode::makeMove(Move * move) {

    MovetreeNode * newHead;
    // Search child moves for move, delete no longer valid subtrees
    for (unsigned int i = 0; i < child_moves.size(); i++) {
	if ((child_moves[i] -> getX() == move -> getX()) &&\
	    (child_moves[i] -> getY() == move -> getY())) {
	    newHead = child_nodes[i];
	}
	else {
	    //delete child_nodes[i];
	}
    }

    return newHead;
}

void MovetreeNode::spawnChildren(int depth) {

    if (board -> hasMoves(side)) {
	for (int i = 0; i < 8; i++) {
	    for (int j = 0; j < 8; j++) {
		Move move(i, j);
		if (board -> checkMove(&move, side)) {
		    Board * newBoard = board -> copy();
		    newBoard -> doMove(&move, side);
		    child_nodes.push_back(				\
			new MovetreeNode(newBoard, playerSide,		\
					 (side == BLACK) ? WHITE : BLACK));
		    child_moves.push_back(new Move(i, j));
		}
	    }
	}
    }
    
    if (depth > 0) {
	for (MovetreeNode* i : child_nodes) {
	    if (i -> side == playerSide) {
		if (i -> getScore() > score) {
		    score = i -> getScore();
		}
	    }
	    else {
		if (i -> getScore() < score) {
		    score = i -> getScore();
		}
	    }
	    i -> spawnChildren(depth - 1);
	}

    }
}
    
