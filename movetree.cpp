#include "movetree.h"

/**
 * Movetree constructor
 */

Movetree::Movetree(Board * board, Side playerSide, Side opponentSide) {

    maxdepth = 2; // move later (should be passed in/time computed

    this -> playerSide = playerSide;
    this -> opponentSide = opponentSide;
    head = new MovetreeNode(board -> copy(), playerSide, playerSide);    
    head -> spawnChildren(maxdepth);

}

Movetree::~Movetree() {
    //delete head;
}

void Movetree::makeMove(Move * move) {
    if (move != NULL) {
	MovetreeNode * temp = head;
	head = temp -> makeMove(move);
	head -> spawnChildren(maxdepth);
	//delete temp;
    }
}

Move *Movetree::getNextMove() {
    return head -> getBestMove();
}
