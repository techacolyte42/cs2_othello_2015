#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include "movetree.h"

using namespace std;

/**
 * @brief our AI player class (note, includes our model of the opponent)
 */
class Player {

private:

    Side playerSide;
    Side opponentSide;
    Board * board;
    Movetree * movetree;

public:

    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
