#ifndef __MOVETREENODE_H__
#define __MOVETREENODE_H__

#include <vector>
#include <stdlib.h>
#include "board.h"

using namespace std;

/**
 * @brief Class containing node in tree of possible moves
 */

class MovetreeNode
{
public:
    MovetreeNode(Board * board, Side playerSide, Side side);
    ~MovetreeNode();

    int getScore();
    Board * getBoard();
    Move * getBestMove();
    MovetreeNode * makeMove(Move * move);
    void spawnChildren(int depth);
    Side side;

private:

    int score;
    Board * board;
    Side playerSide;

    int * heuristic;

    vector<Move*> child_moves;
    vector<MovetreeNode*> child_nodes;
    
};

#endif
