#include "player.h"
#include <iostream>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;


    if (side == BLACK) {
	playerSide = BLACK;
	opponentSide = WHITE;
    }
    else {
	playerSide = WHITE;
	opponentSide = BLACK;
    }

    board = new Board();
    movetree = new Movetree(board, playerSide, opponentSide);

}

/*
 * Destructor for the player.
 */
Player::~Player() {
    //delete movetree;
    //delete board;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {

    Move * nextMove;

    movetree -> makeMove(opponentsMove);
    nextMove = movetree -> getNextMove();
    
    if (nextMove != NULL) {
	movetree -> makeMove(nextMove);
    }

    return nextMove;
}
