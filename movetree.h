#ifndef __MOVETREE_H__
#define __MOVETREE_H__

#include <vector>
#include "movetreenode.h"

using namespace std;

/**
 * @brief Class encapsulating a tree of possible moves
 */
class Movetree {

public:

    Movetree(Board * board, Side playerSide, Side opponentSide);
    ~Movetree();

    void makeMove(Move * move);
    Move * getNextMove();

private:

    MovetreeNode * head;
    int maxdepth;
    Side playerSide;
    Side opponentSide;

};

#endif
