#### CS 2 Othello Tournament

### The Doge of Venice
### Connor Wilson

## Making a tournament worthy bot

Disclaimer: I'm not going to claim that this bot will be remotely tournament
worthy. Especially since as submitted it uses a depth 2 tree and the tournament
is already running. Now that it compiles with a tree, next step would be
clean up bugs (occassionally can't figure out that there's a legal corner move),
get working out to depth 4, then add pruning to get further depth faster,
then implement game stages, and so on. Also, fixed segfaulting by removing
all memory cleanup....

# Game Stages
We differentiate 3 different game stages - early game, middle game,
late game.

The bot should be able to distinguish these game stages. In stage one, we run
off an opening book approximately 500 megabytes in size. This could just be 
a trie/map thing. Given the moves up to this point, here's the move to take.

When we exhaust the opening book, we've reached the midgame. Here we are running
minimax using a weighted heuristic. This is standard minimax with pruning to
save on space.

At some point, we can project to the end of the game. At this point, we want
to dump the weighted heuristic and start actually counting stones, our heuristics
weight for potentialities, once we're projecting to the end of the game (or close
to the end of the game), we no longer have to guess eventualities - we can count
precisely what's going to happen. If we don't switch to raw count, our algo is going
to go for high weight spaces at the expense of capturing many spaces.

# Heuristics
Right now I'm using heuristics used in other othello programs. It would
be nice to run an analysis of relative space occurences in wins vs losses
and use that as a heuristic, especially since I'm using bot heuristics for vs
human players, and it's unclear whether the point is for those bots to actually
win vs just play a good game.

#
